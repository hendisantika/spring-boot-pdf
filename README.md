# Spring Boot PDF

Type this in your terminal : `gradle clean bootRun`

Open your browser :

http://localhost:8080

![Home Page](img/home.png "Home Page")

http://localhost:8080/sample

![Sample Page](img/sample.png "Sample Page")

http://localhost:8080/download

![Download Page](img/download.png "Download Page")