package com.hendisantika.springbootpdf.services;

import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-pdf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/19/17
 * Time: 6:36 AM
 * To change this template use File | Settings | File Templates.
 */

@Service("dataService")
public class DataService {
    public String sayHello(String someone){
        return "hello "+someone;
    }
}
