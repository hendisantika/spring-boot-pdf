package com.hendisantika.springbootpdf.controllers;

import com.hendisantika.springbootpdf.api.GlobalConfig;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-pdf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/19/17
 * Time: 6:46 AM
 * To change this template use File | Settings | File Templates.
 */

@Controller
//@RequestMapping(value = "/")
public class HomeController {
    @GetMapping(value = "/")
    public String index(ModelMap globalModel){
        globalModel.put("mobile", GlobalConfig.readProperty("form.mobile"));
        globalModel.put("email", GlobalConfig.readProperty("form.email"));
        globalModel.put("name", GlobalConfig.readProperty("form.name"));
        globalModel.put("mode", "html");
        return "simpleForm";
    }

    /**
     * Handle request to download a PDF document
     */
    @GetMapping("/sample")
    public ModelAndView downloadSample() {
        Map<String,Object> globalModel = new HashMap<>();
        globalModel.put("status",true);
        globalModel.put("code", "data");
        globalModel.put("message", "data");

        return new ModelAndView("simplePDF", globalModel);
    }

    @GetMapping("/download")
    public ModelAndView downloadForm() {
        Map<String,Object> globalModel = new HashMap<>();
        globalModel.put("mobile", GlobalConfig.readProperty("form.mobile"));
        globalModel.put("email", GlobalConfig.readProperty("form.email"));
        globalModel.put("name", GlobalConfig.readProperty("form.name"));
        globalModel.put("mode", "pdf");

        // return a view which will be resolved by an excel view resolver
        return new ModelAndView("simpleFormPDF", globalModel);
    }
}
