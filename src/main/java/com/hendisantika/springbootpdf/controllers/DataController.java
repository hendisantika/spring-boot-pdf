package com.hendisantika.springbootpdf.controllers;

import com.hendisantika.springbootpdf.services.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-pdf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/19/17
 * Time: 6:45 AM
 * To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping(value = "data")
public class DataController {
    @Autowired
    private DataService dataService;

    @GetMapping
    public Map index(){
        Map<String,Object> resultMap = new HashMap<>();

        resultMap.put("status",true);
        resultMap.put("code","data");
        resultMap.put("message","data");
        return resultMap;
    }

    @GetMapping(value = "hello/{someone:.+}")
    public Map sayHello(@PathVariable String someone){
        Map<String,Object> resultMap = new HashMap<>();

        resultMap.put("status",true);
        resultMap.put("code",someone);
        resultMap.put("message",dataService.sayHello(someone));
        return resultMap;
    }
}
