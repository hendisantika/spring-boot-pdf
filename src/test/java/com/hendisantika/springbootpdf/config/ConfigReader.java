package com.hendisantika.springbootpdf.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-pdf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/19/17
 * Time: 6:53 AM
 * To change this template use File | Settings | File Templates.
 */
public class ConfigReader {
    private static Properties theProps = null;
    public static String readProperty(String propertyName){
        if(theProps == null){
            theProps = loadProperties();
        }

        assert theProps != null;
        return theProps.getProperty(propertyName);
    }

    private static Properties loadProperties(){
        Properties theProps = new Properties();
        try {
            InputStream in = ClassLoader.getSystemResourceAsStream("config/files.properties");
            theProps.load(in);
        } catch (IOException e) {
            return null;
        }
        return theProps;
    }
}
