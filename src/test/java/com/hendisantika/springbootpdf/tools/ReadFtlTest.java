package com.hendisantika.springbootpdf.tools;

import com.hendisantika.springbootpdf.config.ConfigReader;
import com.hendisantika.springbootpdf.utils.FreemarkerUtils;
import com.hendisantika.springbootpdf.utils.PdfFileUtils;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-pdf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/19/17
 * Time: 7:06 AM
 * To change this template use File | Settings | File Templates.
 */
public class ReadFtlTest {
    @Test
    public void fileTest(){
        String htmlContent = loadHtmlContent();

        File pdfFile = new File(ConfigReader.readProperty("path.save.file"));
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(pdfFile);
            //OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, "UTF-8");
            PdfFileUtils.savePdf(fileOutputStream, htmlContent);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } /*catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }*/
    }

    private String loadHtmlContent() {
        Map<String,String> dataMap = new HashMap<>();
        dataMap.put("pageTitle", "LiuWill");
        URL fileResource = ReadFtlTest.class.getResource(ConfigReader.readProperty("path.ftl.direction"));
        System.out.println(fileResource.getFile());
        return FreemarkerUtils.loadFtlHtml(new File(fileResource.getFile()), ConfigReader.readProperty("path.ftl.file"), dataMap);
    }
}
